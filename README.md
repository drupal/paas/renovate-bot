# Renovate bot


## What is Renovate?

The Renovate Bot is a CLI tool that can be used with your Git repositories to automate the update of dependencies. It creates a pull (or merge for Gitlab) request with the most recent version of any outdated dependecies it finds within the repository.

## How do I set up the Renovate bot for another repo?

This instance of the Renovate bot is being run with GitLab pipelines, which means that we need to modify the existing .gitlab-ci.yml file to make any changes in the general Renovate configuration of our bot. The repositories to which Renovate has access are set in the ```RENOVATE_EXTRA_FLAGS``` variable. 

How the file looks currently (monitor dependencies for this repo along with the drupal distribution one):
```
variables:
    RENOVATE_EXTRA_FLAGS: --platform=gitlab --git-url https://gitlab.cern.ch/drupal/paas/renovate-bot drupal/paas/cern-drupal-distribution --onboarding=true 
```

Along with the existing 2 repos, add the URL or relative direction fo your project and you're ready to go!

## Run Renovate outside of scheduled time
Access this repository in the GitLab webpage, go to the CI/CD section on the left menu and clic on Schedules and the "RENOVATE bot schedule" should appear. Clic on the Play button, this will re-run the last pipeline on the master branch.

*NOTE*: If the pipeline has multiple repositories, this will trigger a review in ALL of them.
If you want to trigger the review in a specific repository, you should run its pipeline *[DOUBLE CHECK THIS]* or follow the instructions for that shown in the "Dependency Dashboard" issue created by Renovate within your repository.

***

## Installation
For self-hosting Renovate in Gitlab with pipelines, all you need to do is copying the [template](https://gitlab.com/renovate-bot/renovate-runner/-/blob/main/templates/renovate.gitlab-ci.yml) from the oficial Renovate documentation and fill in the desired variables with your repository information (i.e. RENOVATE_EXTRA_FLAGS).

*NOTE*:  For more details on how and what can be updated on the initial setup PR for renovate in your repo, see the "self-hosted configuration" link on previous *Useful links* section.

## Configuration

- The `RENOVATE_TOKEN` variable is added to the CI variables and is configured as a personal access token for the user @cdduser

User role: `developer` 

Token scope: 
- `api`
- `write_repository`
- `read_registry`

Token: Renovate Bot, Expiration date: 2025-11-01

Read more: https://docs.renovatebot.com/modules/platform/gitlab/

### Troubleshooting
For the initial Renovate configuration PR, it's necessary that users pay attention to permitted characters in branch names. The dafault creates the "renovate/configure" branch, which can be changed with the environment variable ```RENOVATE_ONBOARDING_BRANCH``` in the .gitlab-ci.yml file (here set as "renovate-config"). 

## Project status *FILL IN BEFORE LEAVING*
If there's any dependency that should be prevented form updating manually, you can add it to the list in teh renovate.json file within it's specific repository.

## Useful links

- [Oficial documentation](https://docs.renovatebot.com/)
- [GitHub tutorial](https://github.com/renovatebot/tutorial)
- [GitLab runner](https://gitlab.com/renovate-bot/renovate-runner)
- [Renovate self-hosted configuration docs](https://docs.renovatebot.com/self-hosted-configuration)
